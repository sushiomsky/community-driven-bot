﻿V 0.3.1 2014 11 02
==================
NEW STUFF:
- BotRaisedOnRiver
- FacingDelayedCbet
- FacingDelayedSecondBarrel

MINOR CHANGES:
Reordered flop, turn and river symbols
Reordered Handlist
Renamed some symbols
Added some comments
Fixes StandardBet

CONFIGRATION:
- FacingDelayedCbet
- FacingDelayedSecondBarrel

V 0.3.0 2014 10 31
=================
NEW STUFF
RIVER PART COMPLETED:
- DelayedSecondBarrel
- ThirdBarrel
- FacingThirdBarrel
- FacingCheckRaiseToThirdBarrel
- FacingRaiseToThirdBarrel

CONFIGURATION:
Added River Configuration

V 0.2.0 2014 10 31
=================
NEW STUFF
TURN PART COMPLETED:
- DelayedCbet
- SecondBarrel
- FacingSecondBarrel
- FacingCheckRaiseToSecondBarrel
- FacingRaiseToSecondBarrel

CONFIGURATION:
Added Turn Configuration

V 0.1.0 2014 10 31
==================
NEW STUFF
FLOP PART COMPLETED:
- FlopCbetIP
- FlopCbetOOP
- FacingDonkBet
- DonkBet
- FacingFlopCbet
- FacingCheckRaiseToCbet
- FacingRaiseToCbet

Added New River Scenarios:
- DelayedSecondBarrel

Added New Useful Symbols:
- OneWayPot
- TwoWayPot
- MultyWayPot

CONFIGURATION:
Added configuration for the Flop Part

FIX SYMBOLS:
- PreflopAggressorCbetOnFlop: now should work.
Thanks to THF for fixing the issue

V 0.0.6 2014 10 30
==================
PREFLOP PART COMPLETED
- All the preflop scenarios have been covered, need to be tested!

FIX Preflop Scenarios Handlist:
- 3Bet IP and OOP
- Squeeze IP and OOP
- Facing3Bet IP and OOP

Preflop Scenarios Covered in this release:
- FacingSqueeze
- Facing3BetBeforeFirstAction
- Facing4Bet
- Facing4BetBeforeFirstAction
- Facing5Bet
- Facing5BetBeforeFirstAction

CONFIGURATION
Hand list for:
- FacingSqueeze
- Facing3BetBeforeFirstAction
- Facing4Bet
- Facing4BetBeforeFirstAction
- Facing5Bet
- Facing5BetBeforeFirstAction

V 0.0.5 2014 10 19
==================
Preflop Scenarios Covered:
- 3Bet IP and OOP
- Squeeze IP and OOP
- Facing3Bet IP and OOP

CONFIGURATION:
Hand list for:
- 3Bet IP and OOP
- Squeeze IP and OOP
- Facing3Bet IP and OOP

V 0.0.4 2014 10 18
==================
NEW STUFF:
START PREFLOP LOGIC (2° Part)
Preflop Scenarios Covered:
- OpenRaiseOrOpenLimp
- LimpOrIsolateLimpers

CONFIGRATION:
Add default hand list:
- Suited Hand: Action taken Raise or Bet
- OffSuited Hand: Action taken Call or Check
- Pair: Action taken Check or Fold

FIX SYMBOLS:
- PreflopAggressorCbetOnFlop

V 0.0.3 2014 10 18
==================
FIX SYMBOLS:
- FacingPotentialOpenRaiseOrLimp, Facing3Bet, FacingSqueeze, FacingRaiseToCbet, FacingSecondBarrel, ThirdBarrel, FacingThirdBarrel, FacingCheckRaiseToThirdBarrel, FacingRaiseToThirdBarrel
- Remove useless code BotsLastAction = None - Thanks to THF

V 0.0.2 2014 10 16
==================
BUG FIXES:
- InPosition = replace old reference from 1 to true

NEW STUFF:
- New Symbols OpponentsPlayingShortStack, OpponentsPlayingDeepStack, Facing5BetBeforeFirstAction, PreflopAggressorCbetOnFlop


V 0.0.1 2014 10 15
==================
- Naming correction - Thanks to Ferfran
- Symbols more readable and better coded - Thanks to THF
- Stolen the OHReleaseNotes Layout - Thanks to...I don't know

V 0.0.0 2014 10 12
==================
Initial Public Release